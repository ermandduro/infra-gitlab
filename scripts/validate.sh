#!/bin/sh

TXT_RED="\e[31m"
TXT_YELLOW="\e[33m"
TXT_GREEN="\e[32m"
TXT_CLEAR="\e[0m"

info() {
  echo -e "${TXT_GREEN}INFO:${TXT_CLEAR} $1"
}
error() {
  echo -e "${TXT_RED}ERROR:${TXT_CLEAR} $1"
}

usage() {
  echo "Usage: $(basename $0) <path>"
}

if [  $# -le 0 -o $# -gt 1 ]
then
  usage
  exit 1
fi

echo $1 | grep "sandbox_account/ci"

if [ $? -eq 0 ]
then
  info "Detected CI context. Checking for APP_NAME and TICKET_NUMBER"
  if [ -z "$APP_NAME" -o -z "$TICKET_NUMBER" ]
  then
    error "The environment variables APP_NAME or TICKET_NUMBER are not set"
    error "APP_NAME: ${APP_NAME}"
    error "TICKET_NUMBER: ${TICKET_NUMBER}"
    exit 1
  else
    info "The environment variables APP_NAME and TICKET NUMBER are set"
    info "APP_NAME: ${APP_NAME}"
    info "TICKET_NUMBER: ${TICKET_NUMBER}"
  fi
else
  info "Not in CI context skipping validation"
fi
