module "sequence_diagrams" {
  source                                = "./modules/project"
  project_name                          = "Sequence Diagram Code"
  project_description                   = "Repo to collect code for sequence diagrams"
  project_namespace_id                  = module.group_backend-chapter.id
  branch_name_regex                     = ""
  packages_enabled                      = true
  protected_branches                    = [
    "main",
  ]
  only_allow_merge_if_pipeline_succeeds = false
  project_variables                     = {
  }
}
