module "pytest-locust-api-automation" {
  source                                           = "./modules/project"
  project_name                                     = "Pytest Locust Api Automation"
  project_description                              = "This a sample project showcasing e2e API automation with Pytest and performance test with Locust"
  project_namespace_id                             = module.group_qa-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "testcafe-ui-automation" {
  source                                           = "./modules/project"
  project_name                                     = "TestCafe UI Automation"
  project_description                              = "This is a sample repo to demonstrate UI automation with TestCafe along with the complete CI setup with all the necessary configurations and test reporting in place."
  project_namespace_id                             = module.group_qa-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "typescript_pact_contract_test" {
  source                                           = "./modules/project"
  project_name                                     = "Typescript Pact Contract Test"
  project_description                              = "This is a sample project to demonstrate consumer driven contract testing using the PACT framework and TypeScript."
  project_namespace_id                             = module.group_qa-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "dast-testing-owasp-zap" {
  source                                           = "./modules/project"
  project_name                                     = "dast-testing-owasp-zap"
  project_description                              = ""
  project_namespace_id                             = module.group_qa-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}
