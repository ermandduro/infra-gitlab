module "ocrlib" {
  source                                           = "./modules/project"
  project_name                                     = "ocrlib"
  project_description                              = "Component that does the OCR on documents"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

module "sail" {
  source                                           = "./modules/project"
  project_name                                     = "sail"
  project_description                              = "Shared AI Library"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

module "delphi-manager" {
  source                                           = "./modules/project"
  project_name                                     = "delphi-manager"
  project_description                              = "Processes batch files for DELPHI, triggers OCR, and runs classification/extraction"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

module "autosourcer-deployer" {
  source                                           = "./modules/project"
  project_name                                     = "autosourcer-deployer"
  project_description                              = "Deploys autosourcer"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

module "autosourcer" {
  source                                           = "./modules/project"
  project_name                                     = "autosourcer"
  project_description                              = "Generate sourcing sheets automatically from a set of files"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

module "delphi-manager-deployments" {
  source                                           = "./modules/project"
  project_name                                     = "delphi-manager-deployments"
  project_description                              = "Kubernetes YAML files for DELPHI-related deployments"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

module "archassets" {
  source                                           = "./modules/project"
  project_name                                     = "ArchAssets"
  project_description                              = ""
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "main"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  lfs_enabled                                      = true
  pages_access_level                               = "disabled"
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
  squash_option      = "default_on"
}

module "archmobile" {
  source               = "./modules/project"
  project_name         = "archmobile"
  project_description  = ""
  project_namespace_id = module.group_legacy.id
  project_variables = {
  }
}

module "infra" {
  source                                           = "./modules/project"
  project_name                                     = "infra"
  project_description                              = "Legacy Infrastructure in Code"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "main"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  lfs_enabled                                      = true
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  container_registry_enabled                       = true
  issues_enabled                                   = true
  only_allow_merge_if_pipeline_succeeds            = false
  packages_enabled                                 = true
  snippets_enabled                                 = true
  wiki_enabled                                     = true
  project_variables = {
  }
  protected_branches = []
}

module "document-class-manager" {
  source                                           = "./modules/project"
  project_name                                     = "document-class-manager"
  project_description                              = "Document Classification Manager"
  project_namespace_id                             = module.group_legacy.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
    "DOCKER_AUTH_CONFIG" = {
      value  = data.sops_file.legacy_secrets.data["document-class-manager.DOCKER_AUTH_CONFIG"],
      masked = false
    }
  }
  protected_branches = []
}

