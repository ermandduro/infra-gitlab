provider "gitlab" {
  token = data.sops_file.infrastructure_secrets.data["infra-gitlab.GITLAB_TOKEN"]
}
