variable "project_name" {
  description = "Name of the GitLab project"
  type        = string
}

variable "project_namespace_id" {
  description = "Namespace ID of project"
  type        = string
}

variable "project_description" {
  description = "GitLab project description"
  type        = string
}

variable "protected_branches" {
  description = "Branches to protect"
  type        = list(string)
  default = [
    "main",
  ]
}

variable "push_access_level" {
  description = "Push access levels for the protected branches. Currently, this is a global settings for all protected branches"
  type        = string
  default     = "no one"
}

variable "merge_access_level" {
  description = "Merge access levels for the protected branches. Currently, this is a global settings for all protected branches"
  type        = string
  default     = "developer"
}

variable "project_variables" {
  description = "Project variable for GitLab project"
  type = map(object({
    value  = string
    masked = bool
  }))
}

variable "default_branch" {
  description = "Default branch"
  type        = string
  default     = "main"
}

variable "branch_name_regex" {
  description = "Branch Name REGEX"
  type        = string
  default     = "(feature|hotfix|bugfix|release)\\/([\\w]{1,5}-[\\d]{1,4}-.*)"
}

variable "commit_message_regex" {
  description = "Commit Message REGEX"
  type        = string
  default     = "([\\w]{1,5}-[\\d]{1,4}.*)"
}

variable "author_email_regex" {
  description = "Author Email  REGEX"
  type        = string
  default     = ".*@architrave.de"
}

variable "initialize_with_readme" {
  description = "Initialize the repo with README"
  type        = bool
  default     = true
}

variable "container_registry_enabled" {
  description = "Enable Container Registry"
  type        = bool
  default     = false
}

variable "lfs_enabled" {
  description = "Enable LFS"
  type        = bool
  default     = false
}

variable "only_allow_merge_if_all_discussions_are_resolved" {
  description = "Only allow merge if all discussions are resolved"
  type        = bool
  default     = true
}

variable "only_allow_merge_if_pipeline_succeeds" {
  description = "Only allow merge if pipeline suceeds"
  type        = bool
  default     = true
}
variable "allow_merge_on_skipped_pipeline" {
  description = "(Optional) Set to true if you want to treat skipped pipelines as if they finished with success."
  type        = bool
  default     = false
}

variable "packages_enabled" {
  description = "Enable packages for repo"
  type        = bool
  default     = false
}

variable "snippets_enabled" {
  description = "Enable snippets for repo"
  type        = bool
  default     = false
}

variable "issues_enabled" {
  description = "Enable issues for repo"
  type        = bool
  default     = false
}

variable "wiki_enabled" {
  description = "Enable wiki for repo"
  type        = bool
  default     = false
}

variable "request_access_enabled" {
  description = "Request access enabled"
  type        = bool
  default     = true
}

variable "approvals_before_merge" {
  description = "Approvals before Merge"
  type        = number
  default     = 1
}

variable "merge_method" {
  description = "Merge Method"
  type        = string
  default     = "ff"
}

variable "pages_access_level" {
  description = "Pages access level"
  type        = string
  default     = "private"
}

variable "squash_option" {
  description = "(Optional) Squash commits when merge request. Valid values are never, always, default_on, or default_off."
  type        = string
  default     = "default_off"
}
