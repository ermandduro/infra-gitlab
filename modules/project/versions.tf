terraform {
  required_providers {
    gitlab = {
      version = "~> 3.8.0"
      source  = "gitlabhq/gitlab"
    }

    sops = {
      source  = "carlpett/sops"
      version = "~> 0.5"
    }
  }
}
