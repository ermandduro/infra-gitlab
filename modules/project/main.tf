resource "gitlab_project" "this" {
  name                                             = var.project_name
  description                                      = var.project_description
  namespace_id                                     = var.project_namespace_id
  snippets_enabled                                 = var.snippets_enabled
  issues_enabled                                   = var.issues_enabled
  wiki_enabled                                     = var.wiki_enabled
  approvals_before_merge                           = var.approvals_before_merge
  default_branch                                   = var.default_branch
  merge_method                                     = var.merge_method
  initialize_with_readme                           = var.initialize_with_readme
  only_allow_merge_if_all_discussions_are_resolved = var.only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = var.only_allow_merge_if_pipeline_succeeds
  allow_merge_on_skipped_pipeline                  = var.allow_merge_on_skipped_pipeline
  remove_source_branch_after_merge                 = true
  pages_access_level                               = var.pages_access_level
  container_registry_enabled                       = var.container_registry_enabled
  lfs_enabled                                      = var.lfs_enabled
  packages_enabled                                 = var.packages_enabled
  request_access_enabled                           = var.request_access_enabled
  push_rules {
    author_email_regex   = var.author_email_regex
    branch_name_regex    = var.branch_name_regex
    commit_message_regex = var.commit_message_regex
  }
  squash_option = var.squash_option
}

resource "gitlab_branch_protection" "this" {
  for_each           = toset(var.protected_branches)
  project            = gitlab_project.this.id
  branch             = each.key
  push_access_level  = var.push_access_level
  merge_access_level = var.merge_access_level
}

resource "gitlab_project_variable" "this" {
  for_each = var.project_variables
  project  = gitlab_project.this.id
  key      = each.key
  value    = each.value["value"]
  masked   = each.value["masked"]
}

resource "gitlab_project_badge" "this" {
  project   = gitlab_project.this.id
  link_url  = "https://gitlab.com/architrave-gmbh/infrastructure/infra-gitlab"
  image_url = "https://gitlab.com/architrave-gmbh/infrastructure/infra-gitlab/-/raw/main/files/terraform.svg"
}
