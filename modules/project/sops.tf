data "sops_file" "infrastructure_secrets" {
  provider    = sops
  source_file = "files/infrastructure-secrets.enc.json"
}
