variable "group_name" {
  description = "Name of the GitLab group"
  type        = string
}

variable "group_path" {
  description = "Path of the GitLab group"
  type        = string
}

variable "group_description" {
  description = "Description of GitLab Group"
  type        = string
  default     = ""
}

variable "parent_id" {
  description = "ID of GitLab Parent Group"
  type        = number
  default     = null
}

variable "project_creation_level" {
  description = "Project Creation Level"
  type        = string
  default     = "maintainer"
}

variable "request_access_enabled" {
  description = "Whether to enable users to request access to the group"
  type        = bool
  default     = false
}

variable "lfs_enabled" {
  description = "Toggle LFS support for the group"
  type        = bool
  default     = true

}
variable "subgroup_creation_level" {
  description = "Allow to create subgroups"
  type        = string
  default     = "owner"
}

variable "group_variables" {
  description = "GitLab Group Variables"
  type = map(object({
    value     = string
    masked    = bool
    protected = bool
  }))
}
variable "share_with_group_lock" {
  description = "Prevent sharing a project with another group within this group"
  type        = bool
  default     = false
}

variable "gitlab_managed_default_branch_protection" {
  description = "Enable this if gitlabs default branch protection should be used.\nThis breaks the managed branch protection for terraform"
  type        = number
  default     = 0

}
