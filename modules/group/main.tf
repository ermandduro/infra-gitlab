resource "gitlab_group" "this" {
  name                      = var.group_name
  path                      = var.group_path
  description               = var.group_description
  parent_id                 = var.parent_id
  project_creation_level    = var.project_creation_level
  request_access_enabled    = var.request_access_enabled
  subgroup_creation_level   = var.subgroup_creation_level
  share_with_group_lock     = var.share_with_group_lock
  lfs_enabled               = var.lfs_enabled
  default_branch_protection = var.gitlab_managed_default_branch_protection
}

resource "gitlab_group_variable" "this" {
  for_each  = var.group_variables
  group     = gitlab_group.this.id
  key       = each.key
  value     = each.value["value"]
  protected = each.value["protected"]
  masked    = each.value["masked"]
}
