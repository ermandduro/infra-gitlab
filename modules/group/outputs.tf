output "id" {
  description = "ID of the created Group"
  value       = gitlab_group.this.id
}
