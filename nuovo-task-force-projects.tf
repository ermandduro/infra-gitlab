module "flexicapture-poc" {
  source                                           = "./modules/project"
  project_name                                     = "FlexiCapture POC"
  project_description                              = "All code, notebooks and scripts related to the FlexiCapture POC."
  project_namespace_id                             = module.group_nuovo-task-force.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

