module "textract-poc" {
  source                                           = "./modules/project"
  project_name                                     = "Textract Poc"
  project_description                              = "textract poc"
  project_namespace_id                             = module.group_team-beakers.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}

module "search-filter-poc" {
  source                                           = "./modules/project"
  project_name                                     = "Search Filter PoC"
  project_description                              = "Pre-filter and post-filter strategy PoC for search"
  project_namespace_id                             = module.group_team-beakers.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}

module "ElasticsearchDataIngester" {
  source                                           = "./modules/project"
  project_name                                     = "EDI"
  project_description                              = "Elasticsearch data ingester of the search service"
  project_namespace_id                             = module.group_team-beakers.id
  branch_name_regex                                = "^[a-zA-Z]{1,5}-[0-9]{1,5}$"
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}

module "advanced-search-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "Advanced Search Frontend"
  project_description                              = "React App For Advanced Search"
  project_namespace_id                             = module.group_team-beakers.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}

module "textract-stack" {
  source                                           = "./modules/project"
  project_name                                     = "Textract Stack"
  project_description                              = "Aws textract lambda stack"
  project_namespace_id                             = module.group_team-beakers.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}
