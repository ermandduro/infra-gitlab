module "autosourcer-output-organizer" {
  source                                           = "./modules/project"
  project_name                                     = "Autosourcer Output Organizer"
  project_description                              = ""
  project_namespace_id                             = module.group_team-eyes.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}

module "parashift-webhooks-poc" {
  source                                           = "./modules/project"
  project_name                                     = "Parashift Webhooks POC"
  project_description                              = ""
  project_namespace_id                             = module.group_team-eyes.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables                                = {}
}

module "api-documentation" {
  source                                           = "./modules/project"
  project_name                                     = "API Documentation"
  project_description                              = ""
  project_namespace_id                             = module.group_team-eyes.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables                                = {}
}

module "ipro_frontend" {
  source                                           = "./modules/project"
  project_name                                     = "ipro_frontend"
  project_description                              = ""
  project_namespace_id                             = module.group_team-eyes.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  project_variables                                = {}
}

module "ipro-lambdas" {
  source                                           = "./modules/project"
  project_name                                     = "iPro Lambdas"
  project_description                              = ""
  project_namespace_id                             = module.group_team-eyes.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  project_variables                                = {}
}