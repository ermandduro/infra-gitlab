module "asset-ingestion" {
  source                                           = "./modules/project"
  project_name                                     = "asset-ingestion"
  project_description                              = ""
  project_namespace_id                             = module.group_cockpit.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "document-ingestion" {
  source                                           = "./modules/project"
  project_name                                     = "document-ingestion"
  project_description                              = ""
  project_namespace_id                             = module.group_cockpit.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "cockpit-frontend"
  project_description                              = ""
  project_namespace_id                             = module.group_cockpit.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
    "SENTRY_DSN" = {
      value  = data.sops_file.cockpit_secrets.data["cockpit-frontend.SENTRY_DSN"],
      masked = false,
    }
  }
}


module "cockpit-ssh-config" {
  source                                           = "./modules/project"
  project_name                                     = "SSH Config"
  project_description                              = ""
  project_namespace_id                             = module.group_devops.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-python-boilerplate" {
  source                                           = "./modules/project"
  project_name                                     = "Python Boilerplate"
  project_description                              = ""
  project_namespace_id                             = module.group_devops.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-server-provisioning" {
  source                                           = "./modules/project"
  project_name                                     = "Server Provisioning"
  project_description                              = ""
  project_namespace_id                             = module.group_devops.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
    "ANSIBLE_VAULT_PASSWORD" = {
      value  = data.sops_file.cockpit_secrets.data["cockpit-server-provisioning.ANSIBLE_VAULT_PASSWORD"],
      masked = false,
    }
  }
}

module "cockpit-dind-fail-investigation" {
  source                                           = "./modules/project"
  project_name                                     = "dind-fail-investigation"
  project_description                              = ""
  project_namespace_id                             = module.group_devops.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-ci-includes" {
  source                                           = "./modules/project"
  project_name                                     = "CI Includes"
  project_description                              = ""
  project_namespace_id                             = module.group_devops.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-gitlab-runner" {
  source                                           = "./modules/project"
  project_name                                     = "GitLab Runner"
  project_description                              = ""
  project_namespace_id                             = module.group_devops.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-node" {
  source                                           = "./modules/project"
  project_name                                     = "node"
  project_description                              = ""
  project_namespace_id                             = module.group_ci-container.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-sync" {
  source                                           = "./modules/project"
  project_name                                     = "sync"
  project_description                              = ""
  project_namespace_id                             = module.group_ci-container.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-yamllint" {
  source                                           = "./modules/project"
  project_name                                     = "yamllint"
  project_description                              = ""
  project_namespace_id                             = module.group_ci-container.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-dind-with-compose" {
  source                                           = "./modules/project"
  project_name                                     = "DinD with Compose"
  project_description                              = ""
  project_namespace_id                             = module.group_ci-container.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-python" {
  source                                           = "./modules/project"
  project_name                                     = "python"
  project_description                              = ""
  project_namespace_id                             = module.group_ci-container.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
    "SONAR_SCANNER_VERSION" = {
      value  = data.sops_file.cockpit_secrets.data["cockpit-python.SONAR_SCANNER_VERSION"],
      masked = false,
    }
  }
}

module "cockpit-dotnet" {
  source                                           = "./modules/project"
  project_name                                     = "dotnet"
  project_description                              = ""
  project_namespace_id                             = module.group_ci-container.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-asset-service-stoplight" {
  source                                           = "./modules/project"
  project_name                                     = "asset-service-stoplight"
  project_description                              = ""
  project_namespace_id                             = module.group_asset-service.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-asset-test-data" {
  source                                           = "./modules/project"
  project_name                                     = "Asset Test Data"
  project_description                              = ""
  project_namespace_id                             = module.group_asset-service.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-asset-service-backend" {
  source                                           = "./modules/project"
  project_name                                     = "asset-service-backend"
  project_description                              = "CRUD management for assets."
  project_namespace_id                             = module.group_asset-service.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  request_access_enabled                           = false
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
    "GOOGLE_MAPS_API_KEY_TEST" = {
      value  = data.sops_file.cockpit_secrets.data["cockpit-asset-service-backend.GOOGLE_MAPS_API_KEY_TEST"],
      masked = false,
    }
  }
}

module "cockpit-languages" {
  source                                           = "./modules/project"
  project_name                                     = "languages"
  project_description                              = ""
  project_namespace_id                             = module.group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-ng-cockpit" {
  source                                           = "./modules/project"
  project_name                                     = "ng-cockpit"
  project_description                              = ""
  project_namespace_id                             = module.group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-search-service-spotlight" {
  source                                           = "./modules/project"
  project_name                                     = "search-service-spotlight"
  project_description                              = "This is a spotlight project to define apis and models related to search service"
  project_namespace_id                             = module.group_search-service.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "utarafdar-added-document-models"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-search-service-backend" {
  source                                           = "./modules/project"
  project_name                                     = "search-service-backend"
  project_description                              = "This repository is for the search backend."
  project_namespace_id                             = module.group_search-service.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-react-cockpit-trial" {
  source                                           = "./modules/project"
  project_name                                     = "react-cockpit-trial"
  project_description                              = ""
  project_namespace_id                             = module.group_cockpit-workspace.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-elasticsearch-asset-ingester" {
  source                                           = "./modules/project"
  project_name                                     = "Elasticsearch Asset Ingester"
  project_description                              = "Consumes messages to ingest assets into Elasticsearch."
  project_namespace_id                             = module.group_elasticsearch-ingesters.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-elasticsearch-document-ingester" {
  source                                           = "./modules/project"
  project_name                                     = "Elasticsearch-Document-Ingester"
  project_description                              = "Consumes document related messages and ingests them in elasticsearch"
  project_namespace_id                             = module.group_elasticsearch-ingesters.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-message-replaying-service" {
  source                                           = "./modules/project"
  project_name                                     = "Message Replaying Service"
  project_description                              = "Utility service for replaying enhanced messages, i.e., republishing them for processing. For local development and potentially also for recovery later."
  project_namespace_id                             = module.group_data-insights-and-integration.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-paco-message-enhancer" {
  source                                           = "./modules/project"
  project_name                                     = "paco-message-enhancer"
  project_description                              = ""
  project_namespace_id                             = module.group_data-insights-and-integration.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-message-processing-service" {
  source                                           = "./modules/project"
  project_name                                     = "Message Processing Service"
  project_description                              = "Processes incoming enhanced PACO messages to insert, update, and delete data."
  project_namespace_id                             = module.group_data-insights-and-integration.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "cockpit-e2e-environment-initializer" {
  source                                           = "./modules/project"
  project_name                                     = "E2E Environment Initializer"
  project_description                              = "Creates resources necessary to spin up the E2E environment."
  project_namespace_id                             = module.group_data-insights-and-integration.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  project_variables = {
  }
}

module "cockpit-dii-slack-cloudwatch-alarms" {
  source                                           = "./modules/project"
  project_name                                     = "DII Slack CloudWatch Alarms"
  project_description                              = "Sends CloudWatch Alarms to Slack."
  project_namespace_id                             = module.group_data-insights-and-integration.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  project_variables = {
  }
}

module "cockpit-dwh" {
  source                                           = "./modules/project"
  project_name                                     = "DWH"
  project_description                              = "Creates resources necessary to spin up the E2E environment."
  project_namespace_id                             = module.group_cockpit.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = false
  container_registry_enabled                       = true
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  project_variables = {
  }
}
