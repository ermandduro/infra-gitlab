terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "gitlab-provisioning"
    key            = "gitlab-provisioning/terraform.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "gitlab-provisioning-lock"
  }
}
