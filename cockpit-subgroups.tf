module "group_devops" {
  source                  = "./modules/group"
  group_name              = "DevOps"
  group_path              = "devops"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_asset-service" {
  source                  = "./modules/group"
  group_name              = "asset-service"
  group_path              = "asset-service"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_ng-apps" {
  source                  = "./modules/group"
  group_name              = "ng-apps"
  group_path              = "ng-apps"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_search-service" {
  source                  = "./modules/group"
  group_name              = "search-service"
  group_path              = "search-service"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_cockpit-workspace" {
  source                  = "./modules/group"
  group_name              = "cockpit-workspace"
  group_path              = "cockpit-workspace"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_elasticsearch-ingesters" {
  source                  = "./modules/group"
  group_name              = "Elasticsearch Ingesters"
  group_path              = "elasticsearch-ingesters"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_data-insights-and-integration" {
  source                  = "./modules/group"
  group_name              = "Data Insights and Integration"
  group_path              = "data-insights-and-integration"
  parent_id               = module.group_cockpit.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_ci-container" {
  source                  = "./modules/group"
  group_name              = "CI Container"
  group_path              = "ci-container"
  parent_id               = module.group_devops.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

