locals {
  architrave_group_id = "7846416"
}

module "architrave_gmbh" {
  source                  = "./modules/group"
  group_name              = "Architrave GmbH"
  group_path              = "architrave-gmbh"
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  lfs_enabled             = false
  group_variables = {
    "MR_CORE_ENV_NAME" = {
      value     = "core-tenant"
      masked    = false,
      protected = false,
    },
    "MR_CORE_ENV_TENANT_MANAGER_PASSWORD" = {
      value = data.sops_file.architrave_gmbh_secrets.data["MR_CORE_ENV_TENANT_MANAGER_PASSWORD"],
      # Ideally it would be masked, but doesn't meet gitlab requirements on masked values
      masked    = false,
      protected = false
    },
    "MR_CORE_ENV_TENANT_MANAGER_USERNAME" = {
      value     = "core-tenant-admin+sandbox@architrave.de"
      masked    = false,
      protected = false
    },
    "MR_CORE_ENV_COGNITO_POOL_CLIENT_ID" = {
      value     = "72hu9hmm8gbkakg8pkk8uu3isf"
      masked    = false,
      protected = false
    }
  }
}

module "group_infrastructure" {
  source                  = "./modules/group"
  group_name              = "infrastructure"
  group_path              = "infrastructure"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_document" {
  source                  = "./modules/group"
  group_name              = "Team Document"
  group_path              = "team-document"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables = {
    "DMS_BACKEND_REPOSITORY_API_TOKEN" = {
      value     = data.sops_file.document_group_secrets.data["group_document.DMS_BACKEND_REPOSITORY_API_TOKEN"],
      masked    = false,
      protected = false,
    }
  }
}

module "group_access" {
  source                  = "./modules/group"
  group_name              = "Team Access "
  group_path              = "team-access"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables = {
    "API_KEY" = {
      value     = data.sops_file.access_group_secrets.data["group_access.API_KEY"],
      masked    = true,
      protected = false,
    }
  }
}

module "group_foundations" {
  source                  = "./modules/group"
  group_name              = "Foundations"
  group_path              = "foundations"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables = {
    "TENANT_USERNAME" = {
      value     = data.sops_file.foundations_group_secrets.data["group_foundations.TENANT_USERNAME"],
      masked    = false,
      protected = false,
    },
    "TENANT_PASSWORD" = {
      value     = data.sops_file.foundations_group_secrets.data["group_foundations.TENANT_PASSWORD"],
      masked    = false,
      protected = false,
    }
  }
}

module "group_cockpit" {
  source                  = "./modules/group"
  group_name              = "Team Cockpit"
  group_path              = "team-cockpit"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables = {
    "COCKPIT_NPM_TOKEN" = {
      value     = data.sops_file.cockpit_group_secrets.data["group_cockpit.COCKPIT_NPM_TOKEN"],
      masked    = false,
      protected = false,
    },
    "MOSAIX_NPM_TOKEN" = {
      value     = data.sops_file.cockpit_group_secrets.data["group_cockpit.MOSAIX_NPM_TOKEN"],
      masked    = false,
      protected = false,
    }
  }
}

module "group_qa-chapter" {
  source                  = "./modules/group"
  group_name              = "QA Chapter"
  group_path              = "qa-chapter"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_frontend-chapter" {
  source                  = "./modules/group"
  group_name              = "Frontend Chapter"
  group_path              = "frontend-chapter"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  lfs_enabled             = false
}

module "group_legacy" {
  source                  = "./modules/group"
  group_name              = "Platform 1"
  group_path              = "platform_1"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_internal-it" {
  source                  = "./modules/group"
  group_name              = "Internal IT"
  group_path              = "internal-it"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_nuovo-task-force" {
  source                  = "./modules/group"
  group_name              = "Nuovo Task Force"
  group_path              = "nuovo-task-force"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_mosaix" {
  source                  = "./modules/group"
  group_description       = "The mosaiX subgroup holds all projects related to the Drees & Sommer mosaiX maintenance project."
  group_name              = "mosaiX"
  group_path              = "mosaix"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  share_with_group_lock   = true
}

module "group_backend-chapter" {
  source                  = "./modules/group"
  group_name              = "Backend Chapter"
  group_path              = "backend-chapter"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
}

module "group_team-beakers" {
  source                  = "./modules/group"
  group_name              = "Team Beakers"
  group_path              = "team-beakers"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  lfs_enabled             = false
}

module "group_team-eyes" {
  source                  = "./modules/group"
  group_name              = "Team Eyes"
  group_path              = "team-eyes"
  parent_id               = local.architrave_group_id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  lfs_enabled             = false
}
