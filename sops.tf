data "sops_file" "architrave_gmbh_secrets" {
  provider = sops
  source_file =  "files/architrave-gmbh-secrets.enc.json"
}

data "sops_file" "infrastructure_secrets" {
  provider    = sops
  source_file = "files/infrastructure-secrets.enc.json"
}

data "sops_file" "document_secrets" {
  provider    = sops
  source_file = "files/document-secrets.enc.json"
}

data "sops_file" "document_group_secrets" {
  provider    = sops
  source_file = "files/document-group-secrets.enc.json"
}

data "sops_file" "access_secrets" {
  provider    = sops
  source_file = "files/access-secrets.enc.json"
}

data "sops_file" "access_group_secrets" {
  provider    = sops
  source_file = "files/access-group-secrets.enc.json"
}

data "sops_file" "cockpit_secrets" {
  provider    = sops
  source_file = "files/cockpit-secrets.enc.json"
}

data "sops_file" "cockpit_group_secrets" {
  provider    = sops
  source_file = "files/cockpit-group-secrets.enc.json"
}

data "sops_file" "legacy_secrets" {
  provider    = sops
  source_file = "files/legacy-secrets.enc.json"
}

data "sops_file" "frontend_chapter_secrets" {
  provider    = sops
  source_file = "files/frontend-chapter-secrets.enc.json"
}

data "sops_file" "foundations_group_secrets" {
  provider    = sops
  source_file = "files/foundations-group-secrets.enc.json"
}
