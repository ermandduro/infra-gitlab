module "shiny-engine" {
  source                                           = "./modules/project"
  project_name                                     = "Shiny Engine"
  project_description                              = ""
  project_namespace_id                             = module.group_frontend-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "shiny-app" {
  source                                           = "./modules/project"
  project_name                                     = "Shiny App"
  project_description                              = ""
  project_namespace_id                             = module.group_frontend-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}


module "ui-library" {
  source                                           = "./modules/project"
  project_name                                     = "UI Library"
  project_description                              = "Shared component library for Architrave Frontends"
  project_namespace_id                             = module.group_frontend-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
    "CHROMATIC_PROJECT_TOKEN" = {
      value  = data.sops_file.frontend_chapter_secrets.data["ui-library.CHROMATIC_PROJECT_TOKEN"],
      masked = false,
    }
    "NPM_TOKEN" = {
      value  = data.sops_file.frontend_chapter_secrets.data["ui-library.NPM_TOKEN"],
      masked = false,
    }
  }
}

module "frontend-ci-templates" {
  source                                           = "./modules/project"
  project_name                                     = "Frontend CI Templates"
  project_description                              = "CI Templates for frontend applications"
  project_namespace_id                             = module.group_frontend-chapter.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  project_variables = {
  }
}
