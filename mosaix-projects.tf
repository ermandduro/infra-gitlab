module "mosaix_web-framework" {
  source                                           = "./modules/project"
  project_name                                     = "web-framework"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_framework.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_ts-metamodel" {
  source                                           = "./modules/project"
  project_name                                     = "ts-metamodel"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_swavi.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_interfacema-core-containers" {
  source                                           = "./modules/project"
  project_name                                     = "interfacema-core-containers"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_dataset-filler-service" {
  source                                           = "./modules/project"
  project_name                                     = "dataset-filler-service"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_eds-paco" {
  source                                           = "./modules/project"
  project_name                                     = "eds-paco"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_authentication" {
  source                                           = "./modules/project"
  project_name                                     = "authentication"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_candy-bag" {
  source                                           = "./modules/project"
  project_name                                     = "candy-bag"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_candy-bag-proxy" {
  source                                           = "./modules/project"
  project_name                                     = "candy-bag-proxy"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_eds-paco-dreso-ssh-tunnel" {
  source                                           = "./modules/project"
  project_name                                     = "eds-paco-dreso-ssh-tunnel"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_metrics-monitoring" {
  source                                           = "./modules/project"
  project_name                                     = "metrics-monitoring"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_dataset-service-api" {
  source                                           = "./modules/project"
  project_name                                     = "dataset-service-api"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_candy-bag-commons" {
  source                                           = "./modules/project"
  project_name                                     = "candy-bag-commons"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_ias" {
  source                                           = "./modules/project"
  project_name                                     = "ias"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_object-service-api" {
  source                                           = "./modules/project"
  project_name                                     = "object-service-api"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_object-search-service" {
  source                                           = "./modules/project"
  project_name                                     = "object-search-service"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_candy-factory.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_interfacema" {
  source                                           = "./modules/project"
  project_name                                     = "interfacema"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_interfacema.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "DresoODB_Dataset_API"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_webgis" {
  source                                           = "./modules/project"
  project_name                                     = "webgis"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_interfacema.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_ng-realestate_db" {
  source                                           = "./modules/project"
  project_name                                     = "ng-realestate_db"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "develop"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_build-environments" {
  source                                           = "./modules/project"
  project_name                                     = "build-environments"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_ng-core" {
  source                                           = "./modules/project"
  project_name                                     = "ng-core"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_ng-docker-image" {
  source                                           = "./modules/project"
  project_name                                     = "ng-docker-image"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "mosaix_languages" {
  source                                           = "./modules/project"
  project_name                                     = "languages"
  project_description                              = ""
  project_namespace_id                             = module.mosaix-group_ng-apps.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}
