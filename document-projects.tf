module "mongo-flask" {
  source                                           = "./modules/project"
  project_name                                     = "Mongo Flask"
  project_description                              = "For testing the integration of Flask with Mongodb"
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "document-service" {
  source                                           = "./modules/project"
  project_name                                     = "document-service"
  project_description                              = ""
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "document-browser-prototype" {
  source                                           = "./modules/project"
  project_name                                     = "Document Browser Prototype"
  project_description                              = ""
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "sample-sns-http-consumer" {
  source                                           = "./modules/project"
  project_name                                     = "Sample SNS HTTP Consumer"
  project_description                              = ""
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "sample-sns-lambda-consumer" {
  source                                           = "./modules/project"
  project_name                                     = "Sample SNS Lambda Consumer"
  project_description                              = ""
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "dms-devenv" {
  source                                           = "./modules/project"
  project_name                                     = "dms-devenv"
  project_description                              = "Development environment for the DMS"
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "dms-backend" {
  source                                           = "./modules/project"
  project_name                                     = "dms-backend"
  project_description                              = ""
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
}

module "dms-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "DMS-Frontend"
  project_description                              = "Frontend application for Architrave's DMS"
  project_namespace_id                             = module.group_document.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
    "SENTRY_DSN" = {
      value  = data.sops_file.document_secrets.data["dms-frontend.SENTRY_DSN"],
      masked = false,
    }
  }
}
