module "tenant-manager-api" {
  source                                           = "./modules/project"
  project_name                                     = "tenant-manager-api"
  project_description                              = "Tenant Manager API"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "tenant-manager" {
  source                                           = "./modules/project"
  project_name                                     = "tenant-manager"
  project_description                              = "Tenant Manager Frontend"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "login" {
  source                                           = "./modules/project"
  project_name                                     = "login"
  project_description                              = "Login Service"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "user-manager-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "User Manager Frontend"
  project_description                              = "User Manager Frontend"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "user-profile-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "User Profile Frontend"
  project_description                              = "User Profile Frontend"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "administration-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "Administration Frontend"
  project_description                              = "Administration Frontend"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "user-manager-backend" {
  source                                           = "./modules/project"
  project_name                                     = "User manager api"
  project_description                              = "User manager api"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 1
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "mapping-sheet-automation" {
  source                                           = "./modules/project"
  project_name                                     = "Mapping Sheet Automation"
  project_description                              = "Automation script for the creation of the mapping sheet in the wake of the sourcing process."
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "access-rights-service" {
  source                                           = "./modules/project"
  project_name                                     = "Access Rights Service"
  project_description                              = "Access Rights Service"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "otel-log-poc" {
  source                                           = "./modules/project"
  project_name                                     = "OTEL log instrumentation PoC"
  project_description                              = "Sample Python app that uses OTEL log Instrumentation"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  issues_enabled                                   = false
  container_registry_enabled                       = false
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}

module "minio-poc" {
  source                                           = "./modules/project"
  project_name                                     = "Minio PoC"
  project_description                              = "PoC for creating alternative to transfer server with Minio"
  project_namespace_id                             = module.group_foundations.id
  branch_name_regex                                = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = false
  default_branch                                   = "main"
  protected_branches                               = ["main"]
  push_access_level                                = "maintainer"
  merge_access_level                               = "maintainer"
  issues_enabled                                   = false
  container_registry_enabled                       = true
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  snippets_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = true
  project_variables                                = {}
}
