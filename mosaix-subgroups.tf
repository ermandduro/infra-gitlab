module "mosaix-group_ng-apps" {
  source                  = "./modules/group"
  group_name              = "ng-apps"
  group_path              = "ng-apps"
  parent_id               = module.group_mosaix.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  share_with_group_lock   = true
}

module "mosaix-group_interfacema" {
  source                  = "./modules/group"
  group_name              = "interfacema"
  group_path              = "interfacema"
  parent_id               = module.group_mosaix.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  share_with_group_lock   = true
}

module "mosaix-group_candy-factory" {
  source                  = "./modules/group"
  group_name              = "candy-factory"
  group_path              = "candy-factory"
  parent_id               = module.group_mosaix.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  share_with_group_lock   = true
}

module "mosaix-group_swavi" {
  source                  = "./modules/group"
  group_name              = "swavi"
  group_path              = "swavi"
  parent_id               = module.group_mosaix.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  share_with_group_lock   = true
}

module "mosaix-group_framework" {
  source                  = "./modules/group"
  group_name              = "framework"
  group_path              = "framework"
  parent_id               = module.group_mosaix.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
  group_variables         = {}
  share_with_group_lock   = true
}
