<!-- Space: FUN -->
<!-- Parent: Technical Documentation -->
<!-- Parent: Infrastructure Documentation -->
<!-- Parent: Platform Consumer Documentation -->
<!-- Title: GitLab Resource Provisioning with Terraform -->

<!-- Include: ../confluence_templates/disclaimer.md -->
# infra-gitlab

This repository contains all GitLab resources defined as code. With this module you can provision resources such as GitLab group, project, branch protection, project variable, push rules etc. Check the examples below to learn how to provision different resources.

## GitLab Group

Adjust the code block below to your group name and add it to the groups.tf file

```
module "group_example" {
  source                  = "./modules/group"
  group_name              = "example"
  group_path              = "example"
  parent_id               = module.group_<example>.id
  project_creation_level  = "developer"
  request_access_enabled  = true
  subgroup_creation_level = "maintainer"
}
```

## GitLab Repo

Replace 'example' with the name of your repo and add it to the appropriate x-projects.tf file, where x is the group name under which your repo should be created.

```
module "example" {
  source               = "./modules/project"
  project_name         = "example"
  project_description  = "Example project description"
  project_namespace_id = module.group_infrastructure.id
  branch_name_regex    = ""
  packages_enabled     = true
  protected_branches   = [
    "main",
  ]
  project_variables = {
  }
}
```

## Project Variable

In order to create project variable(s) for existing repository, project_variables block has to be added to the repo, secret block has to be added accordingly as sops secret with key=value pair to the *-secrets.enc.json file like described below.

1. Add project_variable block to x-projects.tf

```
project_variables = {
  "EXAMPLE_VAR" = {
    value  = data.sops_file.<group>_secrets.data["<example_repository>.EXAMPLE_VAR"],
    masked = true
  }
}
```

2. Edit *-secrets.enc.json and add the block below with your secret

```
sops *-secret.enc.json
```

```
{
    "<example_repository>": {
        "EXAMPLE_VAR": "qwerty123",
    }
}
```

## Branch protection

You can define one or more branch protections for same repository my defining branch names as comma separated values like shown below

```
protected_branches = [
  "main",
  "develop",
  ]
```

## Push Rules 

In order to configure push rules for your repo, please add folowwing block to the repos provisioning.

```
  push_rules {
    author_email_regex   = "some_regex"
    branch_name_regex    = "some_regex"
    commit_message_regex = "some_regex"
  }

```

Usefull docs about the resources that can be provisioned with this module:
 - [GitLab Group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group)
 - [GItLab Project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project)
 - [GitLab Project Branch Protection](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection)
 - [GitLab Project Variable](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable)

