module "aws-access-services" {
  source                                           = "./modules/project"
  project_name                                     = "aws-access-services"
  project_description                              = "Monorepo for Cloud IaC and Microservices"
  project_namespace_id                             = module.group_access.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = false
  allow_merge_on_skipped_pipeline                  = true
  project_variables = {
    "ADMIN_EMAIL" = {
      value  = data.sops_file.access_secrets.data["aws-access-services.ADMIN_EMAIL"],
      masked = false,
    }
    "ADMIN_PASSWORD" = {
      value  = data.sops_file.access_secrets.data["aws-access-services.ADMIN_PASSWORD"],
      masked = false,
    }
    "NPM_TOKEN" = {
      value  = data.sops_file.access_secrets.data["aws-access-services.NPM_TOKEN"],
      masked = false,
    }
  }
}

module "auth-module" {
  source                                           = "./modules/project"
  project_name                                     = "Auth Module"
  project_description                              = ""
  project_namespace_id                             = module.group_access.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  allow_merge_on_skipped_pipeline                  = true
  project_variables = {
    "NPM_TOKEN" = {
      value  = data.sops_file.access_secrets.data["auth-module.NPM_TOKEN"],
      masked = false,
    }
  }
}

module "tenant-manager-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "tenant-manager-frontend"
  project_description                              = ""
  project_namespace_id                             = module.group_access.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  allow_merge_on_skipped_pipeline                  = true
  project_variables = {
    "NPM_TOKEN" = {
      value  = data.sops_file.access_secrets.data["tenant-manager-frontend.NPM_TOKEN"],
      masked = false,
    }
    "REACT_APP_API_KEY" = {
      value  = data.sops_file.access_secrets.data["tenant-manager-frontend.REACT_APP_API_KEY"],
      masked = false,
    }
  }
}

module "user-profile" {
  source                                           = "./modules/project"
  project_name                                     = "User Profile"
  project_description                              = ""
  project_namespace_id                             = module.group_access.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  allow_merge_on_skipped_pipeline                  = true
  project_variables = {
    "ADMIN_EMAIL" = {
      value  = data.sops_file.access_secrets.data["user-profile.ADMIN_EMAIL"],
      masked = false,
    }
    "ADMIN_PASSWORD" = {
      value  = data.sops_file.access_secrets.data["user-profile.ADMIN_PASSWORD"],
      masked = false,
    }
    "API_KEY" = {
      value  = data.sops_file.access_secrets.data["user-profile.API_KEY"],
      masked = false,
    }
    "NPM_TOKEN" = {
      value  = data.sops_file.access_secrets.data["user-profile.NPM_TOKEN"],
      masked = false,
    }
  }
}

module "user-management" {
  source                                           = "./modules/project"
  project_name                                     = "User Management"
  project_description                              = ""
  project_namespace_id                             = module.group_access.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = false
  allow_merge_on_skipped_pipeline                  = true
  project_variables = {
    "ADMIN_EMAIL" = {
      value  = data.sops_file.access_secrets.data["user-management.ADMIN_EMAIL"],
      masked = false,
    }
    "ADMIN_PASSWORD" = {
      value  = data.sops_file.access_secrets.data["user-management.ADMIN_PASSWORD"],
      masked = false,
    }
    "API_KEY" = {
      value  = data.sops_file.access_secrets.data["user-management.API_KEY"],
      masked = false,
    }
    "NPM_TOKEN" = {
      value  = data.sops_file.access_secrets.data["user-management.NPM_TOKEN"],
      masked = false,
    }
  }
}

module "platform-migration" {
  source                                           = "./modules/project"
  project_name                                     = "platform-migration"
  project_description                              = ""
  project_namespace_id                             = module.group_access.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  packages_enabled                                 = true
  default_branch                                   = "master"
  protected_branches                               = []
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_all_discussions_are_resolved = false
  only_allow_merge_if_pipeline_succeeds            = false
  project_variables = {
  }
}

module "permissions-poc" {
  source               = "./modules/project"
  project_name         = "permissions-poc"
  project_description  = "PoC repo for permissions concept"
  project_namespace_id = module.group_access.id
  branch_name_regex    = ""
  packages_enabled     = true
  protected_branches = [
    "main",
  ]
  project_variables = {
  }
}

module "translations-poc" {
  source                 = "./modules/project"
  project_name           = "translations-poc"
  project_description    = "PoC repo for translations service using Lokalize"
  project_namespace_id   = module.group_access.id
  branch_name_regex      = ""
  commit_message_regex   = ""
  author_email_regex     = ""
  approvals_before_merge = 0
  merge_method           = "merge"
  packages_enabled       = true
  default_branch         = "main"
  protected_branches = [
    "main",
  ]
  project_variables = {
  }
}
