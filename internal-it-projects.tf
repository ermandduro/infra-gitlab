
module "autopkg-recipes" {
  source                                           = "./modules/project"
  project_name                                     = "AutoPkg Recipes"
  project_description                              = ""
  project_namespace_id                             = module.group_internal-it.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  approvals_before_merge                           = 0
  merge_method                                     = "merge"
  project_variables = {
  }
  protected_branches = []
}

