module "infra-gitlab" {
  source               = "./modules/project"
  project_name         = "infra-gitlab"
  project_description  = "Provisioning of Gitlab resources via Terraform"
  project_namespace_id = module.group_infrastructure.id
  branch_name_regex    = ""
  packages_enabled     = true
  protected_branches   = ["main"]
  project_variables = {
    "GITLAB_TOKEN" = {
      value  = data.sops_file.infrastructure_secrets.data["infra-gitlab.GITLAB_TOKEN"],
      masked = true,
    }
  }
  approvals_before_merge = 0
}

module "infra_definition" {
  source                                           = "./modules/project"
  project_name                                     = "infra_definition"
  project_description                              = "Our infrastructure in code"
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  initialize_with_readme                           = false
  packages_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
  squash_option      = "default_on"
}

module "sample-tenant-context-service" {
  source                                           = "./modules/project"
  project_name                                     = "sample-tenant-context-service"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  initialize_with_readme                           = false
  container_registry_enabled                       = true
  packages_enabled                                 = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "sample-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "sample-frontend"
  project_description                              = "Sample frontend application to show off CI/CD pipelines"
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  initialize_with_readme                           = false
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "infra-dev-envs" {
  source                                           = "./modules/project"
  project_name                                     = "infra-dev-envs"
  default_branch                                   = "master"
  project_description                              = "The project is for infrastructure development environments"
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "customer-setup" {
  source                     = "./modules/project"
  project_name               = "Customer Setup"
  project_description        = ""
  project_namespace_id       = module.group_infrastructure.id
  branch_name_regex          = ""
  author_email_regex         = ""
  commit_message_regex       = ""
  issues_enabled             = true
  container_registry_enabled = true
  lfs_enabled                = true
  packages_enabled           = true
  wiki_enabled               = true
  snippets_enabled           = true
  project_variables = {
  }
  protected_branches = []
  squash_option      = "always"
}

module "infra-tools" {
  source                                           = "./modules/project"
  project_name                                     = "infra-tools"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "sample-backend" {
  source                                = "./modules/project"
  project_name                          = "sample-backend"
  project_description                   = "Sample backend service to show off CI/CD pipelines"
  project_namespace_id                  = module.group_infrastructure.id
  branch_name_regex                     = ""
  author_email_regex                    = ""
  commit_message_regex                  = ""
  container_registry_enabled            = true
  lfs_enabled                           = true
  packages_enabled                      = true
  only_allow_merge_if_pipeline_succeeds = false
  project_variables = {
  }
  protected_branches = []
  squash_option      = "always"
}

module "sample-react-frontend" {
  source                     = "./modules/project"
  project_name               = "Sample React Frontend"
  project_description        = ""
  default_branch             = "master"
  project_namespace_id       = module.group_infrastructure.id
  branch_name_regex          = ""
  author_email_regex         = ""
  commit_message_regex       = ""
  issues_enabled             = true
  container_registry_enabled = true
  lfs_enabled                = true
  packages_enabled           = true
  wiki_enabled               = true
  snippets_enabled           = true
  project_variables = {
  }
  protected_branches = []
}

module "infra-helm-charts" {
  source                                           = "./modules/project"
  project_name                                     = "infra-helm-charts"
  project_description                              = "A collection of helm charts for deploying 'standard' architrave applications with the least amount of effort and knowledge needed."
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "dockerhub-authentication-tester" {
  source                                           = "./modules/project"
  project_name                                     = "Dockerhub Authentication Tester"
  project_description                              = "Fairly small image that can be used to show that dockerhub authentication works."
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "docker-in-docker" {
  source                                           = "./modules/project"
  project_name                                     = "docker-in-docker"
  project_description                              = "Provide a docker in docker container, that includes docker-ecr tools for pushing build images"
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "ci-templates" {
  source                 = "./modules/project"
  project_name           = "ci-templates"
  project_description    = "Repository for Team Cloud CI templates"
  project_namespace_id   = module.group_infrastructure.id
  branch_name_regex      = ""
  author_email_regex     = "@architrave\\.de$"
  commit_message_regex   = ""
  lfs_enabled            = true
  request_access_enabled = false
  project_variables = {
  }
  protected_branches = []
}

module "auth-request-backend" {
  source                     = "./modules/project"
  project_name               = "auth-request-backend"
  project_description        = "This service implements the [ngx_http_auth_request_module](http://nginx.org/en/docs/http/ngx_http_auth_request_module.html) interface for Cognito."
  project_namespace_id       = module.group_infrastructure.id
  branch_name_regex          = ""
  author_email_regex         = ""
  commit_message_regex       = ""
  issues_enabled             = true
  container_registry_enabled = true
  lfs_enabled                = true
  packages_enabled           = true
  wiki_enabled               = true
  snippets_enabled           = true
  project_variables = {
  }
  protected_branches = []
  squash_option      = "always"

}

module "gitops-playground" {
  source                                           = "./modules/project"
  project_name                                     = "GitOps playground"
  project_description                              = ""
  default_branch                                   = "master"
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "test_nested_pipeline_level_1" {
  source                                           = "./modules/project"
  project_name                                     = "test_nested_pipeline_level_1"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "test_nested_pipeline_level_2" {
  source                                           = "./modules/project"
  project_name                                     = "test_nested_pipeline_level_2"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "test_nested_pipeline_level_3" {
  source                                           = "./modules/project"
  project_name                                     = "test_nested_pipeline_level_3"
  project_description                              = "…"
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "test_nested_pipeline_level_4" {
  source                                           = "./modules/project"
  project_name                                     = "test_nested_pipeline_level_4"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "infra_definition_pipeline_playground" {
  source                                           = "./modules/project"
  project_name                                     = "infra_definition_pipeline_playground"
  project_description                              = "Testing the pipeline/trigger function"
  project_namespace_id                             = module.group_infrastructure.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "demo-frontend" {
  source                                           = "./modules/project"
  project_name                                     = "demo-frontend"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}

module "recycling-is-good" {
  source                                           = "./modules/project"
  project_name                                     = "Recycling-is-good"
  project_description                              = ""
  project_namespace_id                             = module.group_infrastructure.id
  default_branch                                   = "master"
  branch_name_regex                                = ""
  author_email_regex                               = ""
  commit_message_regex                             = ""
  issues_enabled                                   = true
  container_registry_enabled                       = true
  lfs_enabled                                      = true
  packages_enabled                                 = true
  wiki_enabled                                     = true
  snippets_enabled                                 = true
  only_allow_merge_if_pipeline_succeeds            = false
  only_allow_merge_if_all_discussions_are_resolved = false
  project_variables = {
  }
  protected_branches = []
}
